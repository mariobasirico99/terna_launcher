import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { Role } from 'src/app/enum/role';
import { AuthenticationService } from 'src/app/services/authentication/authentication.service';
import { UserService } from 'src/app/services/user/user.service';
import { first } from 'rxjs';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
@Component({
  selector: 'app-settings-page',
  templateUrl: './settings-page.component.html',
  styleUrls: ['./settings-page.component.css']
})
export class SettingsPageComponent implements OnInit, AfterViewInit {
  user:any;
  status = false;
  @ViewChild(MatPaginator)
  paginator!: MatPaginator;
  @ViewChild(MatSort)
  sort!: MatSort;

  loading =false;
  displayedColumns: string[] = ['name', 'role','action'];
  dataSource:  MatTableDataSource<any>;;
  dataLength: any = 0;
  users :any;
  constructor(private authenticationService: AuthenticationService,
    private userService : UserService
    ) {
      this.authenticationService.user.subscribe((x) => {
        this.user = x
      });
      this.dataSource =new MatTableDataSource()
    this.userService
      .getAll()
      .pipe(first())
      .subscribe((response) => {
        this.dataLength = response.length
        this.users = response
        this.dataSource = new MatTableDataSource(this.users);
        this.dataSource.paginator = this.paginator
        this.dataSource.sort = this.sort
      });
  }
      ngAfterViewInit() {
        setTimeout(() => this.dataSource.paginator = this.paginator);
        this.dataSource.sort = this.sort;
      }
  ngOnInit(): void {
    
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource!.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  get isAdmin() {
    return this.user && this.user.role === Role.Admin;
  }
  changeStatus(){
    //ipcRenderer.send('request-mainprocess-action');
    //ipc.send('openApp');
    /*
    this.userService.update(3).pipe().subscribe(data=>{
      console.log(data)
    })
    */
  }
}
