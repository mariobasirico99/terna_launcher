import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private router: Router, private http: HttpClient) { }
  getAll(){
    return this.http.get<any>(`${environment.apiUrl}/users/getAll`)
  }
  update(id: number){
    return this.http.put<any>(`${environment.apiUrl}/users/${id}`,{})
  }
}
