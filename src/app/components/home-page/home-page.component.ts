import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import {IpcRenderer} from 'electron';
import { Shell} from 'electron';
import { HomePageModalComponent } from '../home-page-modal/home-page-modal.component';
declare global {
  interface Window {
    require: (module: 'electron') => {
      ipcRenderer: IpcRenderer
      shell: Shell
    };
  }
}

const { ipcRenderer } = window.require('electron');
const { shell } = window.require('electron');
var imagesJson = require("src/assets/config/imageConfig.json")
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {
  list:any[] =[
    { 
      id:1,
      link: "mailto:///",
      name: "Mail",
      type:"App"
    },
    {
      id:2,
      link: "https://www.google.com",
      name: "Google",
      type:"App"

    },
    {
      id:3,
      link: "https://www.google.com",
      name: "Apertura link-web in Terna Launcher",
      type:"Web"

    },
    {
      id:4,
      link: "https://www.google.com",
      name: "Apertura link-web in pagina esterna",
      type:"App"
    },
    {
      id:5,
      link: "https://www.google.com/",
      name: "Apertura link-web in Terna Launcher",
      type:"Web"

    },
    {
      id:6,
      link: "https://www.google.com",
      name: "Apertura link-web in Terna Launcher",
      type:"Web"

    },
    {
      id:7,
      link: "https://www.google.com",
      name: "Apertura link-web in pagina esterna",
      type:"App"
    },
    {
      id:8,
      link: "https://www.google.com/",
      name: "Apertura link-web in Terna Launcher",
      type:"Web"

    }
  ]
  loading=false;
  sourceFolder = "../"
  image = imagesJson

  responsiveOptions:any;


  constructor(
    public dialog: MatDialog
  ) { 
    this.responsiveOptions = [
      {
          breakpoint: '1024px',
          numVisible: 3,
          numScroll: 3
      },
      {
          breakpoint: '768px',
          numVisible: 2,
          numScroll: 2
      },
      {
          breakpoint: '560px',
          numVisible: 1,
          numScroll: 1
      }
  ];
  }

  ngOnInit(): void {
  }
  openApp(){

    ipcRenderer.send('openApp',{link:"C://Program Files//StarUML//StarUML.exe"});
    //window.open(url)
    //shell.openExternal('https://www.google.com/')
  }
  openUrl(){
    ipcRenderer.send('openUrl',{link:"https://www.google.com/"});
  }
  
  openDialog() {
    const dialogRef = this.dialog.open(HomePageModalComponent,{
      data: {
        data: this.list,
      },
    });

  
    dialogRef.beforeClosed().subscribe(result => {
      this.list = result?result:this.list
    console.log("Finish");
    
    });
  }
}


