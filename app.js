//const { app, BrowserWindow } = require('electron')

const electron = require("electron")
const app = electron.app
const BrowserWindow = electron.BrowserWindow
const Menu = electron.Menu
const url = require("url");
const path = require("path");
var ipcMain = electron.ipcMain;
var child = require('child_process')
    //const { Menu } = require('electron');
let mainWindow

function createWindow() {

    mainWindow = new BrowserWindow({
        width: 1200,
        height: 800,
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
            preload: __dirname + '/preload.js'
        },
    })

    mainWindow.loadURL(
        url.format({
            pathname: path.join(__dirname, `/dist/terna-launcher-app/index.html`),
            protocol: "file:",
            slashes: true
        })
    );
    mainWindow.webContents.openDevTools();
    mainWindow.maximize()
    mainWindow.on('closed', function() {
        mainWindow = null
    })
}
app.on('ready', function() {
    createWindow()
    const template = [{
        label: "Ritorna al Launcher",
        click: function() {
            returnToLauncher()
        }
    }]

    const body = [{
        label: "body"
    }]
    const menu = Menu.buildFromTemplate(template)
    Menu.setApplicationMenu(menu)
})
app.on('window-all-closed', function() {
    if (process.platform !== 'darwin') app.quit()
})

app.on('activate', function() {
    if (mainWindow === null) createWindow()
})

function returnToLauncher() {
    mainWindow.loadURL(
        url.format({
            pathname: path.join(__dirname, `/dist/terna-launcher-app/index.html`),
            protocol: "file:",
            slashes: true
        })
    );
}

ipcMain.on('openApp', (event, arg) => {
    let executablePath = arg.link;
    child.execFile(executablePath, function(err, data) {
        event.returnValue = data;
    });

});
ipcMain.on('openUrl', (event, arg) => {
    electron.shell.openExternal(arg.link)
});
/*
const oppapp = () => {
    electron.shell.openExternal('https://www.google.com/')
}
module.exports = { oppapp }
*/
