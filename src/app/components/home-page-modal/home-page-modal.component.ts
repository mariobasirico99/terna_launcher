import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HomePageComponent } from '../home-page/home-page.component';

@Component({
  selector: 'app-home-page-modal',
  templateUrl: './home-page-modal.component.html',
  styleUrls: ['./home-page-modal.component.css']
})
export class HomePageModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<HomePageModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data:HomePageComponent[]
  ) { }
  
  list:any[] = []
  ngOnInit(): void {
    this.item(this.data)
  }

  item(data:any){
    this.list = data.data
  }
  
  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.list, event.previousIndex, event.currentIndex);
  }
  
  savePreferences(list:any){
    this.dialogRef.close(list)
  }
}
