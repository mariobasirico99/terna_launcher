import { Role } from '../enum/role';

export class FakeData {  
  static users = [
    {
      id: 1,
      username: 'admin',
      password: 'admin',
      role: Role.Admin,
    },
    {
      id: 2,
      username: 'user',
      password: 'user',
      role: Role.User,
    },
    {
      id: 3,
      username: 'user3',
      password: 'user3',
      role: Role.User,
    },
    {
      id: 4,
      username: 'user4',
      password: 'user4',
      role: Role.User,
    },
    {
      id: 5,
      username: 'user5',
      password: 'user5',
      role: Role.User,
    },
    {
      id: 6,
      username: 'user6',
      password: 'user6',
      role: Role.User,
    },
    {
      id:7,
      username: 'user7',
      password: 'user7',
      role: Role.User,
    },
    {
      id: 8,
      username: 'user8',
      password: 'user8',
      role: Role.User,
    },
    {
      id: 9,
      username: 'user9',
      password: 'user9',
      role: Role.User,
    },
    {
      id: 10,
      username: 'user10',
      password: 'user10',
      role: Role.User,
    },
    {
      id: 11,
      username: 'user11',
      password: 'user11',
      role: Role.User,
    },
    {
      id: 12,
      username: 'user12',
      password: 'user12',
      role: Role.User,
    },
  ]
}